//

import { BillDTO } from "@/arq_hex/settle/_domain/dto/BillDTO";

export interface IPortBill {

    getBillById(idBill: string): BillDTO | undefined;
    getListBillMeetActive(idMeet: string): Array<BillDTO>;
    getListBillMeetClose(idMeet: string): Array<BillDTO>;
    createBill(idMeet: string, bill: BillDTO): void;
    updateBill(idMeet: string, bill: BillDTO): void;
    deleteBill(idMeet: string, idBill: string): void;
    resetBillStore(): void;
    getDefaultBill(): BillDTO;

}