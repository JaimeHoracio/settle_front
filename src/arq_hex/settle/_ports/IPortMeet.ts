//

import { MeetDTO } from "@/arq_hex/settle/_domain/dto/MeetDTO";

export interface IPortMeet {

    getListHistorialMeet(): Array<MeetDTO>,
    getListActivesMeet(): Array<MeetDTO>,
    getMeetActive(): MeetDTO | undefined,
    setMeetActive(meet: MeetDTO): Promise<MeetDTO>,
    updateMeetActive(): void,
    createMeet(name: string): Promise<MeetDTO>,
    updateMeet(meet: MeetDTO): Promise<void>,
    closeMeet(idMeet: string): Promise<void>,
    removeMeetActive(name: string): void,
    removeMeetHistorial(name: string): void,
    resetMeets(): void,
    saveCurrentStateUserLogin(): void,
    selectMeetToAddFromServer(idMeet: string): Promise<MeetDTO | undefined>,

}