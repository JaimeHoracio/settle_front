//

import { useBillStore } from "@/arq_hex/settle/_domain/store/BillStore";
import { IPortBill } from "@/arq_hex/settle/_ports/IPortBill";
import { BillDTO } from "@/arq_hex/settle/_domain/dto/BillDTO";

export class BillAdapter implements IPortBill {

    private static _instance: BillAdapter;

    public static getInstance() {
        if (this._instance === undefined) {
            this._instance = new BillAdapter();
        }
        return this._instance;
    }

    getBillById(idBill: string): BillDTO | undefined {
        const bill = useBillStore().getBillByIdStore(idBill) as BillDTO[];
        return bill.length > 0 ? bill[0] : undefined;
        //throw new Error("Method not implemented.");
    }

    public getListBillMeetActive(idMeet: string): Array<BillDTO> {
        return useBillStore().getCurrentListBillByMeetActive(idMeet);
    }

    public getListBillMeetClose(idMeet: string): Array<BillDTO> {
        return useBillStore().getCurrentListBillByMeetClose(idMeet);
    }

    public createBill(idMeet: string, bill: BillDTO): void {
        useBillStore().createBillStore(idMeet, bill);
        //throw new Error("Method not implemented.");
    }

    public updateBill(idMeet: string, bill: BillDTO): void {
        useBillStore().updateBillStore(idMeet, bill);
        //throw new Error("Method not implemented.");
    }

    public deleteBill(idMeet: string, idBill: string): void {
        useBillStore().deleteBill(idMeet, idBill);
        //throw new Error("Method not implemented.");
    }

    resetBillStore(): void {
        useBillStore().resetBill();
        //throw new Error("Method not implemented.");
    }

    getDefaultBill() {
        return useBillStore().getDefaultBill()
    }

}