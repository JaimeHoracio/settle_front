//

// Lib para clonar en profundidad
import { IPortMeet } from "@/arq_hex/settle/_ports/IPortMeet";
import { useMeetStore } from '@/arq_hex/settle/_domain/store/MeetStore';
import { MeetDTO } from "@/arq_hex/settle/_domain/dto/MeetDTO";

export default class MeetAdapter implements IPortMeet {

    private static _instance: MeetAdapter;

    public static getInstance() {
        if (this._instance === undefined) {
            this._instance = new MeetAdapter();
        }
        return this._instance;
    }

    // Methods
    selectMeetToAddFromServer(idMeet: string): Promise<MeetDTO | undefined> {
        return useMeetStore().selectMeetToAddFromServer(idMeet);
    }

    getListHistorialMeet(): MeetDTO[] {
        return useMeetStore().meetStore.historyMeetList;
    }

    getListActivesMeet(): MeetDTO[] {
        return useMeetStore().meetStore.activesMeetList;
        //throw new Error("Method not implemented.");
    }

    getMeetActive(): MeetDTO | undefined {
        //return getMeetActiveStore.value;
        return useMeetStore().meetStore.meetActive;
        //throw new Error("Method not implemented.");
    }

    async setMeetActive(meet: MeetDTO): Promise<MeetDTO> {
        //return getMeetActiveStore.value;
        useMeetStore().meetStore.meetActive = meet
        return meet;
        //throw new Error("Method not implemented.");
    }

    async createMeet(name: string): Promise<MeetDTO> {
        return await useMeetStore().createMeetStore(name);

        //throw new Error("Method not implemented.");
    }

    updateMeetActive(): void {
        //return getMeetActiveStore.value;
        const meet = useMeetStore().meetStore.meetActive;
        if (meet) {
            this.updateMeet(meet);
        }
        //throw new Error("Method not implemented.");
    }

    updateMeet(meet: MeetDTO): Promise<void> {
        meet.updated = new Date();
        return useMeetStore().updateMeetStore(meet);
        //throw new Error("Method not implemented.");
    }

    closeMeet(idMeet: string): Promise<void> {
        return useMeetStore().closeMeetStore(idMeet);
        //throw new Error("Method not implemented.");
    }

    removeMeetActive(idMeet: string): void {
        useMeetStore().removeMeetActivesStore(idMeet);
        //throw new Error("Method not implemented.");
    }

    removeMeetHistorial(idMeet: string): void {
        useMeetStore().removeMeetHistorialStore(idMeet);
        //throw new Error("Method not implemented.");
    }

    resetMeets(): void {
        useMeetStore().resetMeets()
    }

    saveCurrentStateUserLogin(): void {
        useMeetStore().saveCurrentStateUserLogin();
    }

}