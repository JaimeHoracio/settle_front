//src/_domain/settle/store/this.meetStore.ts

// https://stackoverflow.com/questions/71452197/how-to-set-up-pinia-getter-in-vue-3-composition-api

import CloneObj from "just-clone";
import { defineStore } from "pinia";

import { executeRemoveMeetServer, executeUpdateMeetServer, executeAddMeetServer, executeCloseMeetServer, executeSelectMeetServer } from "@/arq_hex/_utils/axios/MeetRequest";
import AlertsUtil from "@/arq_hex/_utils/alerts/AlertsUtil";
import { MeetDTO } from '@/arq_hex/settle/_domain/dto/MeetDTO';
import { useUserLoginStore } from "@/arq_hex/gral/_domain/store/UserLoginStore";
import { ErrorDto } from "@/arq_hex/gral/_domain/dto/ErrorDto";
import { isRunningServer } from "@/arq_hex/_utils/axios/AxiosGral";
import { useBillStore } from "@/arq_hex/settle/_domain/store/BillStore";
import { MeetContainerDTO } from "../dto/MeetContainerDTO";
import { BillDTO } from "../dto/BillDTO";

interface MeetState {
    meetStore: MeetContainerDTO
}

const defaultMeetContainer: MeetContainerDTO = { meetActive: undefined, activesMeetList: new Array<MeetDTO>(), historyMeetList: new Array<MeetDTO>() };

export const useMeetStore = defineStore('meetStore', {

    state: (): MeetState => {
        return {
            meetStore: defaultMeetContainer
        }
    },
    getters: {

    },
    actions: {

        //Actions
        getMeetActiveByIdStore(idMeet: string): MeetDTO | undefined {
            const meet = this.meetStore.activesMeetList.find((m) => m.idMeet === idMeet);
            if (!meet) {

                console.log("***ERROR***** getMeetActiveByIdStore this.meetStore", meet)

                AlertsUtil.toastNoMeetActive();
                return undefined
            }
            return meet as MeetDTO;
        },

        getMeetCloseByIdStore(idMeet: string): MeetDTO {
            const meet = this.meetStore.historyMeetList.filter((m) => m.idMeet === idMeet);
            if (!meet) {

                console.log("***ERROR***** getMeetCloseByIdStore this.meetStore", meet)

                AlertsUtil.toastNoMeetActive()
            }
            return meet[0];
        },

        async selectMeetToAddFromServer(idMeet: string) {
            const userLogin = useUserLoginStore().userStore;
            if (await isRunningServer()) {

                return executeSelectMeetServer(idMeet, userLogin.token as string)
                    .then((response) => {
                        const error = response as ErrorDto;
                        if (error.message) {
                            AlertsUtil.toastMessage(error.message as string);
                        } else {
                            return response as MeetDTO;
                        }
                    });
            } else {
                if (!userLogin.isGuest) {
                    useUserLoginStore().userStore.isAllowedSave = true;
                    AlertsUtil.toastNotNetwork();
                }
            }
            return undefined;
        },

        async createMeetStore(nameMeet: string) {

            const meet = new MeetDTO(nameMeet);

            this.$patch((state) => {
                state.meetStore.meetActive = meet
                //Agrego el nuevo encuentro al listado de activos
                state.meetStore.activesMeetList.push({ idMeet: meet.idMeet, name: meet.name, active: meet.active, created: meet.created, updated: meet.updated, listBill: meet.listBill })
            })
            //this.meetStore.meetActive = meet;
            //this.meetStore.activesMeetList.push(meet)

            //Envio al servidor el encuentro creado
            const userLogin = useUserLoginStore().userStore;
            if (await isRunningServer()) {
                executeAddMeetServer(userLogin.email as string, CloneObj(meet), userLogin.token as string)
                    .then((response) => {
                        const error = response as ErrorDto;
                        if (error.message) {
                            AlertsUtil.toastMessage(error.message as string);
                        }
                    });
            } else {
                if (!userLogin.isGuest) {
                    useUserLoginStore().userStore.isAllowedSave = true;
                    AlertsUtil.toastNotNetwork();
                }
            }
            //Devuelvo el encuentro creado para inicializar la estructura del usuario logueado.
            return meet;
        },

        async updateMeetStore(meet: MeetDTO) {
            //Actualizo el encuentro que estoy trabando

            if (meet) {
                const listActives = this.meetStore.activesMeetList.map(m => m.idMeet === meet.idMeet ? meet : m);
                this.$patch((state) => {
                    state.meetStore.meetActive = meet
                    //Agrego el nuevo encuentro al listado de activos
                    state.meetStore.activesMeetList = listActives
                })
                //this.meetStore.meetActive = meet;
                //Tambien actualizo en el listado de activos para que se refleje el cambio.
                //this.meetStore.activesMeetList = listActives

                //Modificar el encuentro en el servidor.
                const userLogin = useUserLoginStore().userStore;
                if (await isRunningServer()) {
                    executeUpdateMeetServer(userLogin.email as string, meet, userLogin.token as string)
                        .then((response) => {
                            const error = response as ErrorDto;
                            if (error.message) {
                                AlertsUtil.toastMessage(error.message as string);
                            }
                        });
                } else {
                    if (!userLogin.isGuest) {
                        useUserLoginStore().userStore.isAllowedSave = true;
                        AlertsUtil.toastNotNetwork();
                    }
                }
            } else {

                console.log("***ERROR***** updatethis.meetStore this.meetStore", meet)

                AlertsUtil.toastNoMeetActive()
            }
        },

        //Ejecutado en el CheckBill
        async closeMeetStore(idMeet: string) {
            //Recupero el encuentro activo
            const meet: MeetDTO | undefined = this.meetStore.meetActive?.idMeet === idMeet ? this.meetStore.meetActive : undefined;
            if (meet) {
                meet.active = false;
                //Actualizo la hora de modificacion.
                meet.updated = new Date();
                // Paso el encuentro a la lista de historial
                meet.listBill = CloneObj(useBillStore().billStore);

                //MODIFO LAS LISTAS ASOCIADAS AL USUARIO
                /**-----------------------------------**/
                const listActives = this.meetStore.activesMeetList.filter(m => m.idMeet !== meet.idMeet);
                this.$patch((state) => {
                    // Reseteo encuentro activo
                    state.meetStore.meetActive = undefined
                    // Lo quito del listado de activos
                    state.meetStore.activesMeetList = listActives
                    // Lo agrego al historial
                    state.meetStore.historyMeetList.push(CloneObj(meet))
                })
                // Lo quito del listado de activos
                //this.meetStore.activesMeetList = this.meetStore.activesMeetList.filter(m => m.idMeet !== meet.idMeet);
                // Lo agrego al historial
                //this.meetStore.historyMeetList.push(CloneObj(meet));
                // Reseteo encuentro activo
                //this.meetStore.meetActive = undefined;
                /**-----------------------------------**/

                //Reflejo el cambio de estado en el servidor.
                const userLogin = useUserLoginStore().userStore;
                if (await isRunningServer()) {
                    executeCloseMeetServer(userLogin.email as string, meet.idMeet, userLogin.token as string)
                        .then((response) => {
                            const error = response as ErrorDto;
                            if (error.message) {
                                AlertsUtil.toastMessage(error.message as string);
                            }
                        });
                } else {
                    if (!userLogin.isGuest) {
                        useUserLoginStore().userStore.isAllowedSave = true;
                        AlertsUtil.toastNotNetwork();
                    }
                }
                //Elimino los gastos hechos
                useBillStore().resetBill();
            } else {

                console.log("***ERROR***** closethis.meetStore this.meetStore", meet)

                AlertsUtil.toastNoMeetActive()
            }
        },

        async removeMeetActivesStore(idMeet: string) {
            //Remuevo del servidor
            await removeMeetServer(idMeet);
            //this.meetStore.activesMeetList = this.meetStore.activesMeetList.filter(obj => obj.idMeet !== idMeet);
            const listActives = this.meetStore.activesMeetList.filter(obj => obj.idMeet !== idMeet);
            this.$patch((state) => {
                // Lo quito del listado de activos
                state.meetStore.activesMeetList = listActives
            })
        },

        async removeMeetHistorialStore(idMeet: string) {
            //Remuevo del servidor
            await removeMeetServer(idMeet);
            //this.meetStore.historyMeetList = this.meetStore.historyMeetList.filter(obj => obj.idMeet !== idMeet);
            const listHistories = this.meetStore.historyMeetList.filter(obj => obj.idMeet !== idMeet);
            this.$patch((state) => {
                // Lo quito del listado de activos
                state.meetStore.historyMeetList = listHistories
            })
        },

        updateListBillByIdMeet(idMeet: string, listBill: BillDTO[]) {
            const meet = this.meetStore.activesMeetList.find(m => m.idMeet === idMeet);
            if (meet) {
                meet.listBill = CloneObj(listBill);
                //NOTA: NO SE INVOCA ACTUALIZAR MEET PORQUE SOLO SE IMPACTA EL PAGO EN LA BASE
                const listActives = this.meetStore.activesMeetList.map(m => m.idMeet === idMeet ? meet : m);
                this.$patch((state) => {
                    // Lo quito del listado de activos
                    state.meetStore.activesMeetList = listActives
                })


                console.log("updateListBillByIdMeet MeetStore", this.meetStore.activesMeetList)

            } else {
                AlertsUtil.toastMessage("Ocurrió un error no se pudo guardar el pago.")
            }
        },

        //Utilizada por el LogOut por si no se actualizó (error network) en el servidor el encuentro activo.
        //NOTA: Solo persiste el ultimo encuentro activo trabajado.
        saveCurrentStateUserLogin() {

            console.log("****************** saveCurrentStateUserLogin********************")

            //Actualizo la hora de modificacion.
            const meet = this.meetStore.meetActive
            if (meet) {
                meet.updated = new Date();
                meet.listBill = useBillStore().billStore;

                const userLogin = useUserLoginStore().userStore
                if (userLogin.isAllowedSave && userLogin.token) {
                    // Marco como que ya fue actualizado el estado en el servidor
                    userLogin.isAllowedSave = false;
                    executeUpdateMeetServer(userLogin.email as string, meet, userLogin.token as string)
                        .then((response) => {
                            const error = response as ErrorDto;
                            if (error.message) {
                                userLogin.isAllowedSave = true;

                            }
                        })
                }
            }
        },

        resetMeets() {
            this.$patch((state) => {
                state.meetStore = defaultMeetContainer;
            })
        }
    }
});

const removeMeetServer = async (idMeet: string): Promise<boolean> => {

    const userLogin = useUserLoginStore().userStore;
    /*----------------------------------------*/
    //Se eliminó exitosamente, lo quito del listado del lado del cliente.
    useUserLoginStore().removeMeetUserLogin(idMeet);
    /*----------------------------------------*/

    if (await isRunningServer()) {
        executeRemoveMeetServer(userLogin.email as string, idMeet, userLogin.token as string)
            .then((response) => {
                const error = response as ErrorDto;
                if (error.message) {
                    AlertsUtil.toastMessage(error.message as string);
                    return false;
                }
            });
    } else {
        if (!userLogin.isGuest) {
            useUserLoginStore().userStore.isAllowedSave = true;
            AlertsUtil.toastNotNetwork();
            return false;
        }
    }
    return true;
}