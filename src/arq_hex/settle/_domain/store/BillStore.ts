//

import CloneObj from "just-clone";

import { defineStore } from "pinia";
import { ref } from "vue";

import AlertsUtil from "@/arq_hex/_utils/alerts/AlertsUtil";
import { BillDTO } from "@/arq_hex/settle/_domain/dto/BillDTO";
import { useUserLoginStore } from "@/arq_hex/gral/_domain/store/UserLoginStore";
import { executeAddBillServer, executeRemoveBillServer, executeUpdateBillServer } from "@/arq_hex/_utils/axios/BillRequest";
import { ErrorDto } from "@/arq_hex/gral/_domain/dto/ErrorDto";
import { isRunningServer } from "@/arq_hex/_utils/axios/AxiosGral";
import { UserDTO } from "@/arq_hex/gral/_domain/dto/UserDTO";
import { useMeetStore } from "./MeetStore";
import { getUniqUuid } from "@/arq_hex/_utils/UtilsNumber";
import { ReceiptContainerDTO } from "../dto/ReceiptContainerDTO";
import { PaymentContainer } from "../dto/PaymentContainerDTO";
import { CurrencyDTO } from "@/arq_hex/gral/_domain/dto/CurrencyDTO";

/*--------------------- Default Bill -------------------------*/
const defaultCurrency: CurrencyDTO = { code: 'UYU', name: 'Peso', country: 'Uruguay' };
const defautlReceipt: ReceiptContainerDTO = {
    amount: 0,
    discount: 0,
    currency: defaultCurrency
}
/*----------------------------------------------*/

export const useBillStore = defineStore('BillStore', {

    state: () => ({
        billStore: ref([] as BillDTO[]),
    }),
    //Los getters pueden verse como computed methods.
    getters: {
        getBillByIdStore: (state) => (idBill: string) => {
            //return state.billStore.filter((obj) => Number(obj.idBill) === Number(idBill));
            return state.billStore.filter((obj) => obj.idBill === idBill);
        },

    },
    actions: {

        getDefaultBill() {
            return {
                idBill: getUniqUuid(),
                reference: '',
                receipt: defautlReceipt,
                listUsersPaid: new Array<PaymentContainer>(),
                created: new Date()
            };
        },

        getCurrentListBillByMeetActive(idMeet: string) {
            const meet = useMeetStore().getMeetActiveByIdStore(idMeet);
            if (meet) {
                this.billStore = meet.listBill;
                return this.billStore;
            } else {
                this.billStore = new Array<BillDTO>();
            }
            return this.billStore;
        },

        getCurrentListBillByMeetClose(idMeet: string) {
            const meet = useMeetStore().getMeetCloseByIdStore(idMeet);
            if (!meet.listBill) {
                meet.listBill = new Array<BillDTO>()
            }
            this.billStore = meet.listBill;
            return this.billStore;
        },

        async createBillStore(idMeet: string, bill: BillDTO) {

            //this.billStore.push(bill);
            this.$patch((state) => {
                state.billStore.push(CloneObj(bill))
            })

            //Guardo el pago en el encuentro que estoy trabajando
            useMeetStore().updateListBillByIdMeet(idMeet, this.billStore);

            //Creo el encuentro en el servidor.
            const userLogin = useUserLoginStore().userStore;
            if (await isRunningServer()) {
                executeAddBillServer(userLogin.email as string, idMeet, bill, userLogin.token as string)
                    .then((response) => {
                        const error = response as ErrorDto;
                        if (error.message) {
                            AlertsUtil.toastMessage(error.message as string);
                        }
                    });
            } else {
                if (!userLogin.isGuest) {
                    useUserLoginStore().userStore.isAllowedSave = true;
                    AlertsUtil.toastNotNetwork();
                }
            }
        },

        async updateBillStore(idMeet: string, bill: BillDTO) {
            const indexOfList = this.billStore.findIndex(obj => obj.idBill === bill.idBill);
            this.billStore[indexOfList] = bill;

            console.log("updateBillStore", idMeet)

            //Guardo el pago en el encuentro que estoy trabajando
            useMeetStore().updateListBillByIdMeet(idMeet, this.billStore);

            //Modificar el encuentro en el servidor.
            const userLogin = useUserLoginStore().userStore;
            if (await isRunningServer()) {
                executeUpdateBillServer(userLogin.email as string, idMeet, bill, userLogin.token as string)
                    .then((response) => {
                        const error = response as ErrorDto;
                        if (error.message) {
                            AlertsUtil.toastMessage(error.message as string);
                        }
                    });
            } else {
                if (!userLogin.isGuest) {
                    useUserLoginStore().userStore.isAllowedSave = true;
                    AlertsUtil.toastNotNetwork();
                }
            }
        },
        async deleteBill(idMeet: string, idBill: string) {
            const indexOfList = this.billStore.findIndex(obj => obj.idBill === idBill);
            this.billStore.splice(indexOfList, 1);

            //Guardo el pago en el encuentro que estoy trabajando
            useMeetStore().updateListBillByIdMeet(idMeet, this.billStore);

            //Elimino el pago en el servidor.
            //Modificar el encuentro en el servidor.
            const userLogin = useUserLoginStore().userStore;
            if (await isRunningServer()) {
                executeRemoveBillServer(userLogin.email as string, idMeet, idBill, userLogin.token as string)
                    .then((response) => {
                        const error = response as ErrorDto;
                        if (error.message) {
                            AlertsUtil.toastMessage(error.message as string);
                        }
                    });
            } else {
                if (!userLogin.isGuest) {
                    useUserLoginStore().userStore.isAllowedSave = true;
                    AlertsUtil.toastNotNetwork();
                }
            }
        },

        getUserBillMeetStore(listUser: UserDTO[], listBill: BillDTO[]) {
            return getUserBillMeet(listUser, listBill);
        },

        resetBill() {
            this.$patch((state) => {
                state.billStore = new Array<BillDTO>();
            })
        }
    }

})

function verifiqueUserList(user: UserDTO, list: UserDTO[]): UserDTO[] {
    const exist = list.find(u => u.name === user.name);
    if (!exist) {
        list.push(user);
    }
    return list;
}

function getUserBillMeet(listUser: UserDTO[], listBill: BillDTO[]): UserDTO[] {
    for (const b of listBill) {
        for (const p of b.listUsersPaid) {
            listUser = verifiqueUserList(p.userPaid.user, listUser);
            for (const d of p.listUsersDebt) {
                listUser = verifiqueUserList(d.user, listUser);
            }
        }
    }
    return listUser;
    //throw new Error("Function not implemented.");
}