//
import { getUniqUuid } from "@/arq_hex/_utils/UtilsNumber";

import { BillDTO } from "./BillDTO";

export class MeetDTO {
    public idMeet: string;
    public active: boolean;
    public name: string | undefined;// Si esta undefined es porque aun no se ha creado
    public created: Date;
    public updated: Date;
    public listBill: Array<BillDTO>;

    constructor(name?: string) {
        this.idMeet = getUniqUuid();
        this.active = true;
        this.name = name;
        this.created = new Date();
        this.updated = new Date();
        this.listBill = new Array<BillDTO>();
    }

}