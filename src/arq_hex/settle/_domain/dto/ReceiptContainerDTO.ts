//

import { CurrencyDTO } from "@/arq_hex/gral/_domain/dto/CurrencyDTO";


export interface ReceiptContainerDTO {

    amount: number;
    discount: number;
    currency: CurrencyDTO;

}