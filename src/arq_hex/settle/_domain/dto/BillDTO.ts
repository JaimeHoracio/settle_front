//src/dto/BillDTO.ts

import { PaymentContainer } from "./PaymentContainerDTO";
import { ReceiptContainerDTO } from "./ReceiptContainerDTO";

export interface BillDTO {

    idBill: string;
    reference: string;
    receipt: ReceiptContainerDTO;
    listUsersPaid: Array<PaymentContainer>;
    created: Date;

}
