//

import { UserDTO } from "@/arq_hex/gral/_domain/dto/UserDTO";

export interface Payment {

    amount: number;
    user: UserDTO;

}
