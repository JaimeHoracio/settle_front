//

import { MeetDTO } from "./MeetDTO";

export interface MeetContainerDTO {

    meetActive: MeetDTO | undefined//Con el que se esta trabajando en el momento
    historyMeetList: MeetDTO[];
    activesMeetList: MeetDTO[];

}