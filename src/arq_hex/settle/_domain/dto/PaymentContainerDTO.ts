//

import { Payment } from "./Payment";

export interface PaymentContainer {

    userPaid: Payment;
    listUsersDebt: Array<Payment>;
}