//

export class ErrorDto {
    message: string | undefined
    codeError: number

    constructor(msg?: string, code?: number) {
        this.message = msg
        this.codeError = code ? code : 0
    }
}