//src/dispense/dto/CurrencyDTO.ts

export interface CurrencyDTO {

    code: string
    num? : number
    name: string
    country: string

}