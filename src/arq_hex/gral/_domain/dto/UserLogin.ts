//src/dto/UserDTO.ts

import { MeetDTO } from "@/arq_hex/settle/_domain/dto/MeetDTO"

export class UserLogin {

    email: string | undefined
    name: string | undefined
    password: string | undefined
    token?: string | undefined
    roles?: string[]
    settle?: {
        listMeet: MeetDTO[]
    }
    isGuest?: boolean
    isAllowedSave?: boolean

    constructor(mail?: string, pass?: string) {
        this.email = mail ? mail : undefined
        this.password = pass ? pass : undefined
        this.isGuest = false
        this.isAllowedSave = false
    }
}