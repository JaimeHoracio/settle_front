//src/store/UserStore.ts

import { defineStore } from "pinia";

import { UserDTO } from "@/arq_hex/gral/_domain/dto/UserDTO";
import { useBillStore } from "@/arq_hex/settle/_domain/store/BillStore";
import { useUserLoginStore } from "./UserLoginStore";

export const useUserStore = defineStore('UserStore', {
    state: () => {
        return {
            userStore: new Array<UserDTO>()
        }
    },
    //Getters
    getters: {
        getUserByName: (state) => {
            return (nameUser: string) => state.userStore.find(obj => obj.name === nameUser);
        }
    },
    //Modifica estados
    actions: {

        isUserLogin(name: string): boolean {
            const nameUserLogin = useUserLoginStore().userStore.name;
            const isUserLogin = this.userStore.find(u => u.name === name && u.name === nameUserLogin)
            return isUserLogin ? true : false;
        },
        isUserInUse(name: string): boolean {
            const listUserInBill = useBillStore().getUserBillMeetStore([], useBillStore().billStore);
            const isInUse = listUserInBill.find(u => u.name === name);
            return isInUse ? true : false;
        },
        async addUserStore(name: string) {
            const user: UserDTO = { name: name };
            this.userStore.push(user);
        },
        async updateUserStore(user: UserDTO) {
            const indexOfUser = this.userStore.findIndex(obj => obj.name === user.name);
            this.userStore[indexOfUser] = user;
        },
        async removeUserStore(name: string) {
            const indexOfUser = this.userStore.findIndex(obj => obj.name === name);
            this.userStore.splice(indexOfUser, 1);
        },
        resetUser() {
            this.$patch((state) => {
                state.userStore = new Array<UserDTO>();
            })
        }
    },
});
