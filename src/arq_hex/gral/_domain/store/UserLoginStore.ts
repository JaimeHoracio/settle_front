//src/store/UserStore.ts

import CloneObj from "just-clone";
import { defineStore } from "pinia";

import AlertsUtil from "@/arq_hex/_utils/alerts/AlertsUtil";

import { UserLogin } from "@/arq_hex/gral/_domain/dto/UserLogin";
import { executeSignIn, executeSignUp } from "@/arq_hex/_utils/axios/AuthRequest";
import { useMeetStore } from "@/arq_hex/settle/_domain/store/MeetStore";
import { useUserStore } from "./UserStore";
import { useBillStore } from "@/arq_hex/settle/_domain/store/BillStore";
import { MeetDTO } from "@/arq_hex/settle/_domain/dto/MeetDTO";
import { BillDTO } from "@/arq_hex/settle/_domain/dto/BillDTO";
import { isRunningServer } from "@/arq_hex/_utils/axios/AxiosGral";
import { ErrorDto } from "../dto/ErrorDto";
import { useCurrencyStore } from "./CurrencyStore";

const defaultUserLogin: UserLogin = { email: undefined, name: undefined, password: undefined, token: undefined, roles: new Array<string>(), isGuest: false, isAllowedSave: false };

export const useUserLoginStore = defineStore('userLogin', {
    state: () => {
        return {
            userStore: defaultUserLogin
        }
    },
    getters: {

    },
    actions: {
        setUserLoginToStore(user: UserLogin) {

            //Agrego el usuario logueado al sistema
            //this.userStore = CloneObj(user);
            this.$patch((state) => {
                state.userStore = user
            })

            //Agrego el usuario autenticado al listado de Usuarios
            //useUserStore().resetUserStore();
            useUserStore().resetUser();
            useUserStore().addUserStore(user.name as string);
        },

        async initSettleUserLogin(meet: MeetDTO) {

            if (meet) {
                //Inicializo la estructura segun el encuentro elegido
                setInitMeetUserLogin(meet);
            } else {

                console.log("***ERROR***** initSettleUserLogin UserLogin: ", meet)

                AlertsUtil.toastNoMeetActive()
            }
        },

        async initSettle_New_MeetUserLogin(meet: MeetDTO) {
            if (this.userStore.settle && !this.userStore.settle.listMeet) {
                this.userStore.settle.listMeet = new Array<MeetDTO>();
            }

            //Inicializo listado de pagos.
            meet.listBill = new Array<BillDTO>();
            //Inicializo la estructura segun el encuentro elegido
            setInitMeetUserLogin(meet);
        },

        async loginUserServer(user: UserLogin) {
            if (await isRunningServer()) {
                return executeSignIn(user);
            } else {
                if (user.isGuest) {
                    //Si es Guest y no hay conexion devuelvo el creado automaticamente.
                    return user;
                } else {
                    return new ErrorDto('Network Error!.', 0);
                }
            }
        },

        async registerUserServer(user: UserLogin) {
            if (await isRunningServer()) {
                return executeSignUp(user);
            } else {
                if (user.isGuest) {
                    //Si es Guest y no hay conexion devuelvo el creado automaticamente.
                    return user;
                } else {
                    return new ErrorDto('Network Error!.', 0);
                }
            }
        },

        cleanListAssociateToUserLogin() {
            //Reseteo todas las listas
            useMeetStore().resetMeets();
            useBillStore().resetBill();
            useUserStore().resetUser();
            useCurrencyStore().resetCurrency();
        },

        removeMeetUserLogin(idMeet: string) {
            if (this.userStore.settle) {
                //this.userStore.settle.listMeet = this.userStore.settle?.listMeet.filter(m => m.idMeet !== idMeet);
                const listActives = this.userStore.settle?.listMeet.filter(m => m.idMeet !== idMeet);
                useMeetStore().$patch((state) => {
                    state.meetStore.activesMeetList = listActives;
                })
            } else {

                console.log("***ERROR***** removeMeetUserLogin UserLogin: ", idMeet)

                AlertsUtil.toastNoMeetActive()
            }

        },

        chargeListMeetFromUser(user: UserLogin) {
            //Cargo los encuentros en sus respectivas listas
            let listActives = new Array<MeetDTO>();
            let listHistorial = new Array<MeetDTO>();
            
            if (user.settle && user.settle.listMeet) {
                //Meets Activo
                const activesFilter: MeetDTO[] | undefined = user.settle.listMeet.filter(m => m.active);
                if (activesFilter && activesFilter.length > 0) {
                    listActives = activesFilter;
                }

                //Historial
                const historialFilter: MeetDTO[] | undefined = user.settle?.listMeet.filter(m => !m.active);
                if (historialFilter && historialFilter.length > 0) {
                    listHistorial = historialFilter;
                }
            }

            //NO ASIGNO NINGUN ENCUENTRO ACTIVO POR DEFECTO
            useMeetStore().$patch((state) => {
                state.meetStore.activesMeetList = listActives;
                state.meetStore.historyMeetList = listHistorial;
            })
        },

        resetUserLogin() {
            this.$patch((state) => {
                state.userStore = defaultUserLogin;
            })
        }

    }

});

const setInitMeetUserLogin = (meet: MeetDTO) => {
    if (meet) {

        useMeetStore().$patch((state) => {
            // Asigno al meet activo el creado o elegido para trabajar.
            state.meetStore.meetActive = meet;
            //state.meetStore.activesMeetList = listActives; NO SE PORQUE ACTUALIZO LISTA ACTIVOS
        })
        // Asigno al meet activo el creado o elegido para trabajar.
        //useMeetStore().meetStore.meetActive = meet;

        //Cargo los Pagos
        useBillStore().$patch((state) => {
            state.billStore = CloneObj(meet.listBill)
        })

        //Agrego a los usuarios que participaron en los pagos
        useUserStore().$patch((state) => {
            state.userStore = useBillStore().getUserBillMeetStore(useUserStore().userStore, meet.listBill)
        })
        //useUserStore().userStore = useBillStore().getUserBillMeetStore(useUserStore().userStore, listBill);
    } else {

        console.log("***ERROR***** setInitMeetUserLogin UserLogin", meet)

        AlertsUtil.toastNoMeetActive()
    }
}

