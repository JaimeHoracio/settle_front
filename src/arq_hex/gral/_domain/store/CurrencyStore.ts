//src/store/UserStore.ts

import { defineStore } from "pinia";

import { CurrencyDTO } from "@/arq_hex/gral/_domain/dto/CurrencyDTO";
import { executeCurrencyServer } from "@/arq_hex/_utils/axios/CurrencyRequest";
import { ErrorDto } from "../dto/ErrorDto";
import AlertsUtil from "@/arq_hex/_utils/alerts/AlertsUtil";
import { ref } from "vue";
import { isRunningServer } from "@/arq_hex/_utils/axios/AxiosGral";
import { useUserLoginStore } from "./UserLoginStore";

export const useCurrencyStore = defineStore('currencyListStore', {
    state: () => {

        //Quitar esto y agregar una pagina para agregar usuarios
        const currencyList = new Array<CurrencyDTO>();
        currencyList.push({ code: 'UYU', name: 'Peso', country: 'Uruguay' });
        currencyList.push({ code: 'USD', name: 'Dollar', country: 'United States' });

        const currencyAll: CurrencyDTO[] = new Array<CurrencyDTO>();
        const defaultCurrency: CurrencyDTO = { code: 'UYU', name: 'Peso', country: 'Uruguay' };
        return {
            defaultCurrency: defaultCurrency,
            currencyListStore: currencyList,
            currencyAllStore: ref(currencyAll)
        }
    },
    //Getters
    getters: {
        getCurrencyById: (state) => {
            return (id_currency: string) => state.currencyListStore.find(obj => obj.code === id_currency);
        }
    },
    //Modifica estados
    actions: {
        async getCurrencyServerFromServer() {
            const userLogin = useUserLoginStore().userStore;
            if (userLogin.token && this.currencyAllStore.length === 0) {

                // Busco todas las monedas al servidor.
                if (!userLogin.isGuest && await isRunningServer()) {
                    await executeCurrencyServer(userLogin.token)
                        .then((response) => {
                            const error = response as ErrorDto;
                            if (error.message) {
                                AlertsUtil.toastMessage(error.message as string);
                            }
                            else {
                                this.currencyAllStore = response as CurrencyDTO[];
                            }
                        });
                } else {
                    if (!userLogin.isGuest) {
                        AlertsUtil.toastNotNetwork();
                    }
                }
            }
            return this.currencyAllStore;
        },
        setDefaultCurrency(currency: CurrencyDTO) {
            this.defaultCurrency = currency;
        },
        setListCurrency(list: Array<CurrencyDTO>) {
            this.currencyListStore = list;
        },
        resetCurrency() {
            this.currencyAllStore = new Array<CurrencyDTO>();
        }

    },
});
