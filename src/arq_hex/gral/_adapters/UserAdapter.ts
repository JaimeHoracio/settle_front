//

import { useUserStore } from "../_domain/store/UserStore";
import { IPortUser } from "@/arq_hex/gral/_ports/IPortUser";
import { UserDTO } from "@/arq_hex/gral/_domain/dto/UserDTO";

const userCtrl = useUserStore();

export class UserAdapter implements IPortUser {

    private static _instance: UserAdapter;

    public static getInstance() {
        if (this._instance === undefined) {
            this._instance = new UserAdapter();
        }
        return this._instance;
    }

    isUserLogin(name: string): boolean {
        return userCtrl.isUserLogin(name);
    }

    isUserInUse(name: string): boolean {
        return userCtrl.isUserInUse(name);
    }
    getListUser(): Array<UserDTO> {
        return userCtrl.userStore;
        //throw new Error("Method not implemented.");
    }
    getUserByName(name: string): UserDTO | undefined {
        return userCtrl.getUserByName(name);
        //throw new Error("Method not implemented.");
    }
    createUser(name: string): void {
        userCtrl.addUserStore(name);
        //throw new Error("Method not implemented.");
    }
    updateUser(user: UserDTO): void {
        userCtrl.updateUserStore(user);
        //throw new Error("Method not implemented.");
    }
    removeUser(name: string): void {
        userCtrl.removeUserStore(name);
        //throw new Error("Method not implemented.");
    }

}