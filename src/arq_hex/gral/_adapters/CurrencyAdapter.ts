//@ts-check

// Lib para clonar en profundidad
import ClonarObj from 'just-clone';

import { CurrencyDTO } from "@/arq_hex/gral/_domain/dto/CurrencyDTO";
import { ICurrencyPort } from "@/arq_hex/gral/_ports/ICurrencyPort";
import { useCurrencyStore } from '@/arq_hex/gral/_domain/store/CurrencyStore';

export class CurrencyAdapter implements ICurrencyPort {

    private static _instance: CurrencyAdapter;

    public static getInstance() {
        if (this._instance === undefined) {
            this._instance = new CurrencyAdapter();
        }
        return this._instance;
    }

    getCurrencyById(id_currency: string) {
        const currency = useCurrencyStore().getCurrencyById(id_currency) as CurrencyDTO;
        return currency;
        //throw new Error("Method not implemented.");
    }

    getCurrencyAllFromServer(): Promise<Array<CurrencyDTO>> {
        return useCurrencyStore().getCurrencyServerFromServer();
    }

    getListCurrency(): CurrencyDTO[] {
        return ClonarObj(useCurrencyStore().currencyListStore);
        //throw new Error("Method not implemented.");
    }

    getDefaultCurrency() {
        return ClonarObj(useCurrencyStore().defaultCurrency);
    }

    setDefaultCurrency(currency: CurrencyDTO) {
        return useCurrencyStore().setDefaultCurrency(currency);
    }

    setListCurrency(list: Array<CurrencyDTO>) {
        return useCurrencyStore().setListCurrency(list);
    }

    resetCurrency() {
        useCurrencyStore().resetCurrency();
    }
}