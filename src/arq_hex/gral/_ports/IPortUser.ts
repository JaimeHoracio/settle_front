//

import { UserDTO } from "@/arq_hex/gral/_domain/dto/UserDTO";

export interface IPortUser {

    isUserLogin(name: string): boolean,
    isUserInUse(name: string): boolean,
    getListUser(): Array<UserDTO>,
    getUserByName(name: string): UserDTO | undefined,
    createUser(name: string): void,
    updateUser(user: UserDTO): void,
    removeUser(name: string): void,

}