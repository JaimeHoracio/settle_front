//

import { UserLogin } from "../_domain/dto/UserLogin";


export interface IPortUserLogin {

    getUserLogin(): UserLogin,

}