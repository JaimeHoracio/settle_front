//

import { CurrencyDTO } from "../_domain/dto/CurrencyDTO";

export interface ICurrencyPort {
    setDefaultCurrency(arg0: CurrencyDTO): unknown;

    getCurrencyById(id_currency: string): CurrencyDTO | undefined;
    getListCurrency(): Array<CurrencyDTO>;
    getCurrencyAllFromServer(): Promise<Array<CurrencyDTO>>;
    getDefaultCurrency(): CurrencyDTO;
    setDefaultCurrency(currency: CurrencyDTO): void;
    setListCurrency(list: Array<CurrencyDTO>): void;

}