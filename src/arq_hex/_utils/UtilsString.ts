

export const limitTextGral = (text: string, length: number): string => {
    return text ? text.substring(0, length).concat(text && text.length > length ? "." : "") : text;
}