//

//const max_Number = 1000;
import { uuid } from 'vue-uuid';

export const getRandomNumber = () => {
    //return Math.floor(Math.random() * max_Number);
    return new Date().getTime();
}

export const getUniqUuid = () => {
    return uuid.v4();
}

export const formatNumberTwoDecimals = (num: number) => {
    return num.toFixed(2) //Math.round(num)
}

export const formatNumberRound = (num: number) => {
    return Math.round(num);
}