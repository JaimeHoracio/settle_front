//
import moment from 'moment';

export const formatDateSimple = (date?: Date): string => {
    return moment(date ? date : new Date()).format("DD-MM-YYYY");
}

export const formatDateSimpleShortTogether = (date?: Date): string => {
    return moment(date ? date : new Date()).format("DDMMYYHHmm");
}

export const formatDateFull = (date?: Date): string => {
    return moment(date ? date : new Date()).format("DD-MM-YYYY HH:mm:ss");
}

export const formatDateFullShort = (date?: Date): string => {
    return moment(date ? date : new Date()).format("DD-MM-YY HH:mm");
}

export const formatDateFullTogether = (date?: Date): string => {
    return moment(date ? date : new Date()).format("DDMMYYYYHHmmss");
}

export const formatHoursSimple = (date?: Date): string => {
    return moment(date ? date : new Date()).format("HH:mm");
}
