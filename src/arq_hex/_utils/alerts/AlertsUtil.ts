//src/utils/AlertsUtil.ts

import { AlertInput, ToastOptions, alertController, toastController } from '@ionic/vue';

export default class AlertsUtil {

    static confirmAlert = async (msg: string) => {
        const alert = await alertController.create({
            header: 'Atención!',
            message: msg,
            buttons: ['OK'],
        });
        await alert.present();
        await alert.onDidDismiss(); // Espera hasta click boton "ok"
    }

    static confirmCancelAlert = async (msgHeader: string, cancelTxt: string, confirmTxt: string) => {
        const alert = await alertController.create({
            header: msgHeader,
            buttons: [
                {
                    text: cancelTxt,
                    role: 'cancel'
                },
                {
                    text: confirmTxt,
                    role: 'confirm'
                },
            ],
        });
        await alert.present();
        const { role } = await alert.onDidDismiss();
        return role === "confirm";
    }

    static inputNumberAlert = async (msgHeader: string, value: number, min?: number, max?: number): Promise<number> => {
        const alert = await alertController.create({
            header: msgHeader,
            backdropDismiss: false,
            inputs: [
                {
                    type: 'number',
                    value: value,
                    min: min,
                    max: max
                }
            ],
            buttons: [
                {
                    text: 'Ok',
                    role: 'ok',
                }
            ]
        });
        await alert.present();
        const { data } = await alert.onDidDismiss();
        const amount = Number(data.values[0]);
        return amount;
    }

    static userPaidAlert = async (listPaid: string[], listDebt: string[]) => {
        const listUsers: AlertInput[] = new Array<AlertInput>()
        for (const paid of listPaid) {
            listUsers.push({
                type: 'checkbox',
                label: paid,
                checked: true,
                disabled: true
            })
        }
        for (const debt of listDebt) {
            listUsers.push({
                type: 'checkbox',
                label: debt,
                checked: false,
                disabled: true
            })
        }
        const alert = await alertController.create({
            header: `${listUsers.length === 0 ? 'Pagadores' : 'Pagador'}`,
            inputs: listUsers,
            buttons: ['OK'],
        });
        await alert.present();
        await alert.onDidDismiss(); // Espera hasta click boton "ok"
    }

    static toastMessage = async (msg: string, duration?: number, option?: ToastOptions) => {
        const toast = await toastController.create({
            message: msg,
            duration: duration ? duration : 1000,
            position: option ? option.position : 'top', //'top''middle''bottom'
        });

        await toast.present();
    }

    static toastNoMeetActive = async (duration?: number, option?: ToastOptions) => {
        const toast = await toastController.create({
            message: 'Error, no seleccionó un encuentro.',
            duration: duration ? duration : 1000,
            position: option ? option.position : 'top', //'top''middle''bottom'
        });

        await toast.present();
    }

    static toastNotNetwork = async (duration?: number) => {
        const toast = await toastController.create({
            message: 'Network Error!.',
            duration: duration ? duration : 1000,
            position: 'top', //'top''middle''bottom'
        });

        await toast.present();
    }

}