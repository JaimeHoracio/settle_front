//
import { ErrorDto } from '@/arq_hex/gral/_domain/dto/ErrorDto';
import axios from 'axios';

//const hostServer = 'https://hache.up.railway.app/api/hache';
//const healthServer = 'https://hache.up.railway.app/actuator/health'

const hostServer = `${process.env.VUE_APP_URL_HOST_SERVER}/api/hache`;
const healthServer = `${process.env.VUE_APP_URL_HOST_SERVER}/actuator/health`

/*############## Generic queries ##############*/
export const isRunningServer = async () => {
    return await axios.get(healthServer)
        .then((response) => {
            return response.data.status === "UP";
        })
        .catch((e) => {
            console.log("Health Server (false):", e.message)
            //No hay conexion
            return false;
        });
}

/*-----------------------------------------------*/

export const convertResultToError = (e: any) => {
    if (e.response) {
        return new ErrorDto(e.response.data.message);
    } else {
        //Si no existe el response es porque no hay conexion
        return new ErrorDto(e.message)
    }
}

/*-----------------------------------------------*/

export const executePOST = async (uri: string, payload: string, token?: string): Promise<any> => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
    }

    return await axios.post(`${hostServer}${uri}`, payload, { headers })
        .then((response) => {
            return (response.data);
        })
        .catch((e) => {
            console.log("Error from server POST: ", e)
            return convertResultToError(e);
        });
}
/*-----------------------------------------------*/

export const executeGET_BODY = async (uri: string, data: string, token: string) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
    }

    return await axios.get(`${hostServer}${uri}`, { headers, data })
        .then((response) => {
            return (response.data);
        })
        .catch((e) => {
            console.log("Error from server GET: ", e)
            return convertResultToError(e);
        });
}

/*-----------------------------------------------*/

export const executeGET = async (uri: string, token: string) => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
    }

    return await axios.get(`${hostServer}${uri}`, { headers })
        .then((response) => {
            return (response.data);
        })
        .catch((e) => {
            console.log("Error from server GET: ", e)
            return convertResultToError(e);
        });
}

/*-----------------------------------------------*/

export const executePUT = async (uri: string, payload: string, token?: string): Promise<any> => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
    }

    return await axios.put(`${hostServer}${uri}`, payload, { headers })
        .then((response) => {
            return (response.data);
        })
        .catch((e) => {
            console.log("Error from server PUT: ", e)
            return convertResultToError(e);
        });
}

/*-----------------------------------------------*/

export const executeDELETE = async (uri: string, data: string, token?: string): Promise<any> => {
    const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
    }

    return await axios.delete(`${hostServer}${uri}`, { headers, data })
        .then((response) => {
            return (response.data);
        })
        .catch((e) => {
            console.log("Error from server DELETE: ", e)
            return convertResultToError(e);
        });
}

/*-----------------------------------------------*/
