//

import { executeGET } from "./AxiosGral";

export const executeCurrencyServer = async (token: string) => {

    return await executeGET('/currency/all', token);
};