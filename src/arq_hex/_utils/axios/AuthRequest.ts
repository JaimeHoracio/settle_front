//

import { UserLogin } from "@/arq_hex/gral/_domain/dto/UserLogin";
import { executePOST } from "./AxiosGral";

export const executeSignUp = async (user: UserLogin) => {
    const payload = {
        email: user.email,
        name: user.name,
        password: user.password
    }
    const jsonPayload = JSON.stringify(payload);
    return await executePOST('/auth/signup', jsonPayload);
}

export const executeSignIn = async (user: UserLogin) => {
    const payload = {
        email: user.email,
        password: user.password
    }
    const jsonPayload = JSON.stringify(payload);
    return await executePOST('/auth/signin', jsonPayload);
}
