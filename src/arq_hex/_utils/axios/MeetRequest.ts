//

import { executeDELETE, executeGET_BODY, executePOST, executePUT } from "./AxiosGral";
import { MeetDTO } from "@/arq_hex/settle/_domain/dto/MeetDTO";



export const executeSelectMeetServer = async (idMeet: string, token: string) => {

    const datos = {
        email: '',
        meet: { idMeet }
    }
    const body = JSON.stringify(datos);

    return await executeGET_BODY('/settle/meet/select', body, token);
};

export const executeAddMeetServer = async (email: string, meet: MeetDTO, token: string) => {

    const payload = {
        email,
        meet
    }
    const body = JSON.stringify(payload);

    return await executePOST('/settle/meet', body, token);
};

export const executeUpdateMeetServer = async (email: string, meet: MeetDTO, token: string) => {

    const payload = {
        email,
        meet
    }
    const body = JSON.stringify(payload);

    return await executePUT('/settle/meet', body, token);
};


export const executeCloseMeetServer = async (email: string, idMeet: string, token: string) => {

    const payload = {
        email,
        meet: {
            idMeet
        }
    }

    const data = JSON.stringify(payload);

    return await executePOST('/settle/meet/close', data, token);
};

export const executeRemoveMeetServer = async (email: string, idMeet: string, token: string) => {

    const payload = {
        email,
        meet: {
            idMeet
        }
    }

    const data = JSON.stringify(payload);

    return await executeDELETE('/settle/meet', data, token);
};