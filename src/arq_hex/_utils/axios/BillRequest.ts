//

import { executeDELETE, executePOST, executePUT } from "./AxiosGral";
import { BillDTO } from "@/arq_hex/settle/_domain/dto/BillDTO";

export const executeAddBillServer = async (email: string, idMeet: string, bill: BillDTO, token: string) => {

    const payload = {
        email,
        idMeet,
        bill
    }
    const body = JSON.stringify(payload);

    return await executePOST('/settle/bill', body, token);
};

export const executeUpdateBillServer = async (email: string, idMeet: string, bill: BillDTO, token: string) => {

    const payload = {
        email,
        idMeet,
        bill
    }
    const body = JSON.stringify(payload);

    return await executePUT('/settle/bill', body, token);
};

export const executeRemoveBillServer = async (email: string, idMeet: string, idBill: string, token: string) => {

    const payload = {
        email,
        idMeet,
        bill: {
            idBill
        }
    }
    const data = JSON.stringify(payload);

    return await executeDELETE('/settle/bill', data, token);
};
