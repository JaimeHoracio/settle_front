import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';

import HomeApp from '@/views/HomeApp.vue';
import settleRoutes from './settleRoutes';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/home',
    component: HomeApp
  },
  {
    path: '/login',
    component: () => import('@/views/LoginApp.vue'),
  },
  {
    path: '/currency-list',
    component: () => import('@/views/gral/CurrencyList.vue')
  },
  ...settleRoutes,
  {
    // '/:catchAll(.*)',
    path: '/:pathMatch(.*)*',
    redirect: 'home'
  },
]

export const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

/*
const path_list_meet = '/settle/main/meet-list';
const path_home = '/home'

router.beforeEach(async (to, from) => { //(to, from, next)
  if (from.fullPath !== path_home && to.fullPath === path_list_meet) {
    router.go(-20);//Borro el historial
    router.push(path_list_meet);
  }
});
*/


