//

export default [
    {
        path: '/settle',
        redirect: '/settle/main/home',
    },
    {
        path: '/settle/main/',
        component: () => import('@/views/settle/SettleMain.vue'),
        children: [
            {
                path: 'home',
                component: () => import('@/views/settle/SettleBody.vue'),
            },
            {
                path: 'meet-add/:id?',
                component: () => import('@/views/settle/MeetAdd.vue'),
                props: true // Habilito recuperar el parametro como propiedad. 
            },
            {
                //Parametro (id) opcional, se uitliza para saber cuando actualizar
                path: 'bill-add/:id?',
                component: () => import('@/views/settle/BillAdd.vue'),
                props: true // Habilito recuperar el parametro como propiedad.
            },
            {
                path: 'check-bill',
                component: () => import('@/views/settle/CheckBill.vue')
            },
            {
                path: 'meet-list',
                component: () => import('@/views/settle/MeetList.vue'),
            },
            {
                path: 'user-list',
                component: () => import('@/views/gral/UserList.vue')
            },
        ]
    },
]
