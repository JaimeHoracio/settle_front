import { createApp } from 'vue';
import App from './App.vue';
import { router } from './router';
import { createPinia } from 'pinia';
import withUUID from "vue-uuid";

// Importacion global de componentes, hay que hacer un uso adecuado.
import {
  IonicVue,
  IonPage,
  IonContent,
  IonHeader,
  IonToolbar,
  IonLabel,
  IonTitle,
  IonMenu,
  IonList,
  IonItem,
  IonCard,
  IonIcon,
  IonButton

} from '@ionic/vue';


/* Core CSS required for Ionic components to work properly */
import '@ionic/vue/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/vue/css/normalize.css';
import '@ionic/vue/css/structure.css';
import '@ionic/vue/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/vue/css/padding.css';
import '@ionic/vue/css/float-elements.css';
import '@ionic/vue/css/text-alignment.css';
import '@ionic/vue/css/text-transformation.css';
import '@ionic/vue/css/flex-utils.css';
import '@ionic/vue/css/display.css';

const pinia = createPinia();

const app = createApp(App)
  .use(pinia)
  .use(router)
  .use(IonicVue)
  .use(withUUID);

app.component('IonPage', IonPage);
app.component('IonContent', IonContent);
app.component('IonHeader', IonHeader);
app.component('IonToolbar', IonToolbar);
app.component('IonTitle', IonTitle);
app.component('IonMenu', IonMenu);
app.component('IonList', IonList);
app.component('IonItem', IonItem);
app.component('IonLabel', IonLabel);
app.component('IonCard', IonCard);
app.component('IonIcon', IonIcon);
app.component('IonButton', IonButton);

//Manejo centralizado de todos los errores de la aplicacion.
app.config.errorHandler = (err, vm, info) => {
  console.log("Deteccion global de errores:");
  console.log(`Mensaje de error: ${err}`);
  console.log(`Componente del error: ${vm?.$options.name}`);
  console.log(`Informacion del error: ${info}`);
}

router.isReady().then(() => {
  app.mount('#app');
});